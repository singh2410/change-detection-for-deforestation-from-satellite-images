## Name
Change detection for deforestation, water reserves from Satellite images.

## Description
Permanent destruction of forests or water reserves to make the land available for other uses is increasing rapidly form last several years. Consequences are related to increased emissions of greenhouse gases, pollution of water, and loss of biodiversity. Change detection for deforestation and water reserves is the problem of detecting the changes in the earth where people are intentionally trying to destroy the forest and water reserves areas. Images collected from Satellite needs real-time change detection of such activities timely, so that, the deforestation and water reserves can be saved before they get destroyed. It can be significantly advantageous if we can detect the changes from the Satellite images but, it is not feasible to track the changes manually in the Satellite images. There is a need of an automated system which can provide some meaningful information by processing large number of images timely and yet accurate. 
Nowadays, Computer Vision and Deep Learning based solutions are becoming popular to detect and track the changes from large number of images. In this research, a novel Deep Learning based change detection method will be exploited for detecting the changes in the Satellite images. One of the challenges in applying Depp Learning based model like Deep belief networks (DBNs) to change detection is, applying such models on large number of images requires lot of processing power(GPU’s) and time. On successful completion of the  project it will deliver the functionality that will enable to detect the changes from Satellite images for deforestation and water reserves.

## Background
Regardless of the increasing awareness of deforestation and its consequences, deforestation and water reserves changes are continuing. Despite the challenges and difficulties, change detection from Satellite images remains an active research area in computer vision in recent years. In the past, many researches have made efforts to analyze the Satellite images to find out the various useful insights about changes in the satellite images over the time. The earlier methods of change detection were based on Image Processing Techniques, image fusion, Fuzzy clustering, Difference Image (DI). In DI, the idea is to generate DI by comparing two images pixel by pixel. Differencing and rationing are the well-known methods for DI. More information about DI can be found at https://ieeexplore.ieee.org/abstract/document/6889510/. The traditional methods are not so accurate in terms of detecting the changes from the satellite images. According to the nature of data processing, Change detection methods can be categorized either as supervised or unsupervised. The supervised method requires a ground truth to derive a suitable training set. The unsupervised approach, performs change detection by making a direct comparison of multi-temporal images without incorporating any additional information. Unsupervised change detection techniques mainly use automatic analysis of change data which may be obtained using multitemporal images. Several Machine learning algorithms like K-Means, FCM (Fuzzy C-Means Clustering) and EM (Expectation Maximization) have also been applied for change detection from the images.
Nowadays lot of research is going on in object detection, object recognition and in change detection from image data using Deep Neural Network (DNN). Environmentalists and researchers are turning to Artificial Intelligence and satellites to combat forest destruction in
real-time that is, in time to stop it. Convolution Neural Network, Deep Belief Network(DBN) and Auto encoders have shown considerably good result in recent years. Deep belief networks(DBNs) are the early proposed deep architectures formed by stacking Restricted Boltzmann machines (RBMs) and will be exploited in this work. DBN is a type of Unsupervised Pretrained Networks (UPNs). More information can be found in Cao G, Wang B, Xavier HC, Yang D, Southworth J. “A new difference image creation method based on deep neural networks for change detection in remote-sensing images”.International Journal of Remote Sensing.
## Methodology
Architecture of the change detection for deforestation/water reserves is shown in fig 1.
Step 1: Data collection and dataset preparation This will involve collection of publicly available Satellite image dataset from available sources.
Step 2: Developing a DBN based change detection model Restricted Boltzmann Machine(RMB) is a single-layered neural network while, DBN is a deep structure formed by stacking RBMs, where the output of the previous layer of RBM serves as the input of the next layer of RBM as shown in fig 2. To detect the changes from Satellite, a DBN based model will be developed. The Satellite images will be inputted to the first later of RBM. The process is repeated from left to right to train all layers of RBM. The model will be trained by some sample training data and then it will be tested by some test data. The feature extraction will be performed automatically by the model. The model will be fine-tuned for various parameter to detect the accurate change detection. New images will be applied to trained and fine-tuned model. Depending on the accuracy of the result more number of layers will be added or model will be trained against more number of epochs. 
Step 3: Training and experimentation on datasets The change detection model will be trained both on the large-scale datasets such as U.S.Landsat Analysis Ready Data (ARD), Sentinel-2 and EuroSAT and dataset that will be populated based on Indian context as part of this project.

![Screenshot from 2021-12-20 07-38-33](https://user-images.githubusercontent.com/76821730/146703073-37e3911f-c744-493b-b53f-3330dae236e3.png)

![Screenshot from 2021-12-20 07-38-48](https://user-images.githubusercontent.com/76821730/146703137-fdaba1d6-7796-4776-923c-8836e370dce1.png)

## Experimental Design:
#### Dataset:
U.S. Landsat Analysis Ready Data (ARD) are one of the most widely used dataset for the detection of deforestation and water reserves. More information about the dataset is available at https://landsat.usgs.gov/ard. Also, there are some other dataset like Sentinel-2 and EuroSAT.

#### Evaluation Measures:
Measures such as accuracy and other measures like False Negative(FN), False Positive(FP),Pearson’s Correlation Coefficient(PCC) will be computed for the proposed deep learning method and will be used to compare with existing methods of change detection.

## Software and Hardware Requirements:
For the development and experimentation of the project, Python based Computer Vision and Deep Learning libraries will be exploited. Specifically, libraries such as OpenCV, Keras, TensorFlow will be used. Training will be conducted on NVIDIA GPUs for training the end-to-end version of DBN based change detection model.

## Usage
This project can be used by meteorological department and other research companies in order to get the proper view of an area so that they may plan or work their upcoming projects accordingly as it is satellite based so the field survillance work can be saved by using this model.

## Support
In case of any query  related to my work you can mail me @ [aarush](aarushkumar100616@gmail.com)

## Roadmap
After the successful completion of my project work I planned to deploy on some cloud platform along with publishing a research paper for the same.

## Authors 
Aarush Kumar

## License
This project would be published under Apache2.0 Community License.

## Project status
Project is under Model developement phase which would laterly be deployed on some  platform.

## Thankyou!!...

